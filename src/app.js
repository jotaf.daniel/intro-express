import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { setupDB } from './db';

export const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.disable('x-powered-by');

export async function bootstrap() {
  const db = await setupDB();

  const handler = (req, res, next) => {
    res.json({ message: 'hello world' });
  };

  app.get('/', handler);

  app.post('/projects', async (req, res) => {
    const { name } = req.body;

    const slug = name.toLowerCase();

    const project = {
      slug,
      boards: [],
    };

    const existingProject = await db.collection('projects').findOne({ slug });

    if (existingProject) {
      res.status(400).json({ error: `project ${slug} already exists` });
    } else {
      await db.collection('projects').insertOne(project);
      res.json({ project });
    }
  });

  const findProject = async (req, res, next) => {
    const { slug } = req.params;

    const project = await db.collection('projects').findOne({ slug });

    if (!project) {
      res.status(400).json({ error: `project ${slug} does not exist` });
    } else {
      req.body.project = project;
      next();
    }
  };

  app.get('/projects/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    res.json({ project });
  });

  app.post('/projects/:slug/boards', findProject, async (req, res) => {
    const { name, project } = req.body;
    const board = { name, tasks: [] };
    project.boards.push(board);

    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ board });
  });

  return db;
}
